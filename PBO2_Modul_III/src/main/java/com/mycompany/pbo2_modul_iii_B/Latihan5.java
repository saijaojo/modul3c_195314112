/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pbo2_modul_iii_B;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import static javax.swing.WindowConstants.EXIT_ON_CLOSE;

public class Latihan5 extends JFrame {

    private static final int FRAME_WIDTH = 300;
    private static final int FRAME_HEIGHT = 200;
    private static final int FRAME_X_ORIGIN = 150;
    private static final int FRAME_Y_ORIGIN = 250;
    private JList list;

    public static void main(String[] args) {
        Latihan5 frame = new Latihan5();
        frame.setVisible(true);
    }

    public Latihan5() {
        Container contentPane;
        JPanel listPanel, okPanel;
        String[] names = {"Ape", "Bat", "Bee", "Cat", "Dog", "Eel", "Fox", "Gnu", "Hen", "Sow", "Yak"};

        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        setTitle("Program Ch14ListSample");
        setLocation(FRAME_X_ORIGIN, FRAME_Y_ORIGIN);

        contentPane = getContentPane();
        contentPane.setBackground(Color.black);
        contentPane.setLayout(new BorderLayout());

        listPanel = new JPanel(new GridLayout(0, 1));
        listPanel.setBorder(BorderFactory.createTitledBorder("Three-letter Animal Name"));

        list = new JList(names);
        listPanel.add(new JScrollPane(list));

        okPanel = new JPanel(new FlowLayout());
        JButton okButton = new JButton("OK");
        okPanel.add(okButton);
        
        contentPane.add(listPanel,BorderLayout.CENTER);
        contentPane.add(okPanel, BorderLayout.SOUTH);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

    }
}


