/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.pbo2_modul_iiic;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

/**
 *
 * @author asus
 */
public class Nomor7 extends JDialog{

    private static final int DIALOG_WIDTH = 650;
    private static final int DIALOG_HEIGHT = 200;
    private static final int DIALOG_X_WIDTH = 150;
    private static final int DIALOG_Y_HEIGHT = 250;
    private JList list;
    
    public static void main(String[] args) {
        Nomor7 dialog = new Nomor7();
        dialog.setVisible(true);
    }

    public Nomor7() {
         Container contentPane;
        JPanel panel1, panel2;
        JLabel label1, label2, label3, label4, label5,label6;
        String [] data = {"Canada","Cina","Denmark","France","Germany","India"
        ,"Norway", "United Kindom", "United States Of America"};
        
        setSize(DIALOG_WIDTH, DIALOG_HEIGHT);
        setTitle("ListDemo");
        setLocation(DIALOG_X_WIDTH, DIALOG_Y_HEIGHT);
        contentPane = getContentPane();
        //contentPane.setBackground(Color.white);
        contentPane.setLayout(new GridLayout(1,2));
        
        panel1 = new JPanel();
        list = new JList(data);
        panel1.add(new JScrollPane(list));
        
        
        panel2 = new JPanel(new FlowLayout());
        ImageIcon img1 = new ImageIcon("canada7.png");
        label1 = new JLabel();
        label1.setIcon(img1);
        ImageIcon img2 = new ImageIcon("indo7.png");
        label2 = new JLabel();
        label2.setIcon(img2);
        ImageIcon img3 = new ImageIcon("germany7.png");
        label3 = new JLabel();
        label3.setIcon(img3);
        ImageIcon img4 = new ImageIcon("india7.png");
        label4 = new JLabel();
        label4.setIcon(img4);
        ImageIcon img5 =  new ImageIcon("united7.png");
        label5 = new JLabel();
        label5.setIcon(img5);
        ImageIcon img6 = new ImageIcon("america7.png");
        label6 = new JLabel();
        label6.setIcon(img6);
        
        panel2.add(label1);
        panel2.add(label2);
        panel2.add(label3);
        panel2.add(label4);
        panel2.add(label5);
        panel2.add(label6);
        
        contentPane.add(panel1);
        contentPane.add(panel2);
        
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
       
    }
}
